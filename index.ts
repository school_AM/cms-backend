import * as groups from "./src/routes/groups";
import * as users from "./src/routes/users";
import * as models from "./src/routes/models";
import * as formats from "./src/routes/format/formats";
import * as convert from "./src/routes/format/convert";
import * as compress from "./src/routes/format/compress";
import * as apikeys from "./src/routes/apikeys";
import * as authenticator from "./src/routes/authenticator";
import * as bodyParser from "body-parser";
// @ts-ignore
import * as serverlessHttp from "serverless-http";
import * as cors from "cors";
import * as express from "express";
import {Request, Response} from "express";
import * as queryHelper from "./src/utils/QueryHelper";
import checkRole from "./src/middleware/validateRole";
import {UserRole} from "./src/data/model/entity/user";

const app = express()
const corsOptions = {
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  exposedHeaders: ["Content-Range", "X-Content-Range", "X-Total-Count"],
}

app.use(cors(corsOptions))

app.use(bodyParser.json({ strict: false, limit: "200mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "200mb" }));

app.use((req: Request, res: Response, next) => {
  req.query = queryHelper.getQueryParams(req.query)
  next()
})

/*
-----GROUPS-----
 */

// Create group
app.post("/groups", [checkRole(UserRole.ADMIN)], groups.createGroup)

// Get group
app.get("/groups/:id", [checkRole(UserRole.ADMIN)], groups.getGroup)

// Get all groups
app.get("/groups", [checkRole(UserRole.ADMIN)], groups.getAllGroups)

// Update group
app.put("/groups/:id", [checkRole(UserRole.ADMIN)], groups.updateGroup)

// Delete group
app.delete("/groups/:id", [checkRole(UserRole.ADMIN)], groups.deleteGroup)

/*
-----USERS-----
 */

// Create user
app.post("/users", [checkRole(UserRole.ADMIN)], users.createUser)

// Get user
app.get("/users/:id", [checkRole(UserRole.ADMIN)], users.getUser)

// Get all users
app.get("/users", [checkRole(UserRole.ADMIN)], users.getAllUsers)

// Update user
app.put("/users/:id", [checkRole(UserRole.ADMIN)], users.updateUser)

// Delete user
app.delete("/users/:id", [checkRole(UserRole.ADMIN)], users.deleteUser)

/*
-----MODELS-----
 */

// Create model
app.post("/models", [checkRole(UserRole.USER)], models.createModel)

// Get model
app.get("/models/:id", [checkRole(UserRole.USER)], models.getModel)

// Get all models
app.get("/models", [checkRole(UserRole.USER)], models.getAllModels)

// Update model
app.put("/models/:id", [checkRole(UserRole.USER)], models.updateModel)

// Delete model
app.delete("/models/:id", [checkRole(UserRole.USER)], models.deleteModel)

/* -----CONVERTERS----- */

// Create format
app.post("/models/:id/convert/to-gltf", [checkRole(UserRole.USER)], convert.toGltf)

/* -----COMPRESS----- */

// Create format
app.post("/models/:id/compress", [checkRole(UserRole.USER)], compress.compress)

/*
-----FORMATS-----
 */

// Create format
app.post("/formats", [checkRole(UserRole.USER)], formats.createFormat)

// Get format
app.get("/formats/:id", [checkRole(UserRole.USER)], formats.getFormat)

// Get all formats
app.get("/formats", [checkRole(UserRole.USER)], formats.getAllFormats)

// Delete format
app.delete("/formats/:id", [checkRole(UserRole.USER)], formats.deleteFormat)

/*
-----API KEY-----
 */

// Create apikey
app.post("/apikeys", [checkRole(UserRole.USER)], apikeys.createApiKey)

// Get apikey
app.get("/apikeys/:id", [checkRole(UserRole.USER)], apikeys.getApiKey)

// Get all apikeys
app.get("/apikeys", [checkRole(UserRole.USER)], apikeys.getAllApiKeys)

// Update apikey
app.put("/apikeys/:id", [checkRole(UserRole.USER)], apikeys.updateApiKey)

// Delete apikey
app.delete("/apikeys/:id", [checkRole(UserRole.USER)], apikeys.deleteApiKey)

/*
-----AUTH-----
*/

// Login
app.post("/auth/login", authenticator.login)

// @ts-ignore
export const handler = serverlessHttp(app);
