import {NextFunction, Request, Response} from "express";
import UserService from "../data/service/userService";
import {UserRole} from "../data/model/entity/user";
import {verify} from "jsonwebtoken";
import JwtPayload from "../data/model/auth/jwtPayload";
import {AuthType} from "../data/model/auth/authType";

const userService = new UserService()

export default function checkRole(role: UserRole) {
  
  return async (req: Request, res: Response, next: NextFunction) => {
    
    try {
  
      // Get the jwt token from the head
      const token = (req.headers.authorization as string).slice(7)
  
      // Try to validate the token and get data
      const jwtPayload = verify(token, process.env.JWT_SECRET!) as JwtPayload;
      res.locals.jwtPayload = jwtPayload;
  
      const roles = []
      
      if (jwtPayload.type === AuthType.USER) {
        // Get the user ID
        const id = jwtPayload.user!.id;
        
        // Get user role from the database
        const user = await userService.get(id);
        
        // add user to response local data
        res.locals.user = user
        
        roles.push(...user.roles)
        
      } else {
        roles.push(UserRole.USER)
      }
  
      // Check if array of authorized roles includes the user's role
      if (roles.findIndex((it) => it === role) > -1) {
        next();
      } else {
        res.status(403).send();
      }
      
    } catch (error) {
      // If token is not valid, respond with 401 (unauthorized)
      res.status(401).send();
    }
  };
};
