import {Request, Response} from "express";
import UserService from "../data/service/userService";
import * as jwt from "jsonwebtoken";
import JwtPayload from "../data/model/auth/jwtPayload";
import {AuthType} from "../data/model/auth/authType";

const userService = new UserService()

/**
 * Login user by email and password.
 *
 * 1. Find user in DB
 * 2. Verify password
 * 3. Generate JWT token
 *
 * @param {e.Request} req
 * @param {e.Response} res
 */
export async function login(req: Request, res: Response) {
  const credentials: { email: string, password: string } = req.body
  
  if (!(credentials.email && credentials.password)) {
    res.sendStatus(400)
    return
  }
  
  try {
    // 1.
    const user = await userService.findByEmail(credentials.email)
    
    // 2.
    if (!user.validatePassword(credentials.password)) {
      res.sendStatus(401)
    }
    
    // 3.
    const payload: JwtPayload = { type: AuthType.USER, user: {id: user.id, email: user.email } }
    const token = jwt.sign(
      payload,
      process.env.JWT_SECRET!,
      { expiresIn: "21d" },
    );
    
    res.json({ token: token, roles: user.roles })
    
  } catch (e) {
    console.log(e)
    res.sendStatus(401)
  }
}

export function generateApiToken(req: Request, res: Response): string {
  const gorupId = req.body.groupId
  
  if (gorupId !== undefined) {
    const payload: JwtPayload = { type: AuthType.TOKEN, group: { id: gorupId } }
    const token = jwt.sign(
      payload,
      process.env.JWT_SECRET!,
      {expiresIn: "20y"},
    );
  
    return token
    
  } else {
    throw new Error("Can't find group which to use.")
  }
}
