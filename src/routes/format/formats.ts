import FormatService from "../../data/service/formatService";
import {FORMAT_GROUP_ID} from "../../data/model/entity/format";
import {getGroupId, isAdmin} from "../../utils/auth";
import {Request, Response} from "express";
import AuthCRUDHelper from "../helpers/AuthCrudHelper";

const CRUD = new AuthCRUDHelper(new FormatService())

const TableName = "format"

export const createFormat = (req: Request, res: Response) => {
  
  if (!isAdmin(res)) {
    req.body.groupId = getGroupId(res)
  }
  
  CRUD.create(TableName, req, res)
}

export const getFormat = (req: Request, res: Response) => {
  CRUD.get(TableName, req, res)
}

export const getAllFormats = (req: Request, res: Response) => {
  
  if (!isAdmin(res)) {
    if (!req.query.filter) {
      req.query.filter = {}
    }
    
    req.query.filter[FORMAT_GROUP_ID] = getGroupId(res)
  }
  
  CRUD.getAll(TableName, req, res)
}

export const deleteFormat = (req: Request, res: Response) => {
  CRUD.remove(TableName, req, res)
}
