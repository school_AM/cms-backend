import {Request, response, Response} from "express";
import FormatService from "../../data/service/formatService";
import {get, upload} from "../../data/s3/S3Service";
import * as fs from "fs";
// @ts-ignore
import * as obj2gltf from "obj2gltf"
import uuid = require("uuid");
import * as AdmZip from "adm-zip"
import Format from "../../data/model/entity/format";

const formatService = new FormatService()

export const toGltf = async (req: Request, res: Response) => {
  const dirPath = "/tmp/" + uuid() + "/"
  
  try {
    // get all formats
    const formats = await formatService.getAll({ modelId: req.params.id })
    
    // find zip format
    const base: Format | undefined = formats.find((it) => it.extension === ".zip")
    const gltfFormat: Format | undefined = formats.find((it) => it.extension === ".gltf")
  
    if (base === undefined) {
      response.status(400).send({ error: "There is no .obj zip archive to convert from." })
      return
      
    } else if (gltfFormat !== undefined) {
      response.status(400).send({ error: ".gltf format already exists." })
      return
  
    } else {
  
      // get original file
      const result = await get(base.s3Key)
      const baseData = result.Body as Buffer
  
      // create temp dir
      fs.mkdirSync(dirPath)
  
      // unzip
      const zip = new AdmZip(baseData)
      zip.extractAllTo(dirPath)
  
      // find name of .obj file
      console.log("paths:", fs.readdirSync(dirPath))
      const objName = fs.readdirSync(dirPath).find((it) => it.endsWith(".obj"))
  
      // convert
      const gltf = await obj2gltf(dirPath + objName)
      const gltfBuffer = Buffer.from(JSON.stringify(gltf));
  
      // save to s3
      const s3Result = await upload(uuid() + ".gltf", gltfBuffer)
  
      // save to DB
      const format: Format = await formatService.create({
        groupId: base.groupId,
        modelId: base.modelId,
        s3Key: s3Result.Key,
        url: s3Result.Location,
        size: gltfBuffer.length,
        extension: ".gltf",
      } as any)
  
      res.status(200).send(format)
    }
    
  } catch (e) {
    console.error(e)
    res.status(500).send("Error converting: " + e)
  } finally {
  
    fs.readdirSync(dirPath).forEach((it) => fs.unlinkSync(dirPath + it))
    fs.rmdirSync(dirPath)
  }
}
