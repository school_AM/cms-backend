import {Request, response, Response} from "express";
import {get, upload} from "../../data/s3/S3Service";
import uuid = require("uuid");
import FormatService from "../../data/service/formatService";
// @ts-ignore
import gltfPipeline = require("gltf-pipeline");
import Format from "../../data/model/entity/format";

const gltfToGlb = gltfPipeline.gltfToGlb;
const formatService = new FormatService()

export const compress = async (req: Request, res: Response) => {
  
  try {
    // get all formats
    const formats = await formatService.getAll({ modelId: req.params.id })
  
    // find gltf format
    const base: Format | undefined = formats.find((it) => it.extension === ".gltf")
    const glbFormat: Format | undefined = formats.find((it) => it.extension === ".glb")
  
    if (base === undefined) {
      response.status(400).send({ error: "There is no .gltf zip archive to convert from." })
      return
    
    } else if (glbFormat !== undefined) {
      response.status(400).send({ error: ".glb format already exists." })
      return
    
    } else {
  
      // get original file
      const result = await get(base.s3Key)
      const baseData = result.Body as Buffer
  
      // draco compress
      const options = {
        dracoOptions: {
          compressionLevel: 10,
        },
      };
  
      const glbBuffer: Buffer = (await gltfToGlb(JSON.parse(baseData.toString()), options)).glb
  
      // save to s3
      const s3Result = await upload(uuid() + ".glb", glbBuffer)
  
      // save to DB
      const format = await formatService.create({
        groupId: base.groupId,
        modelId: base.modelId,
        s3Key: s3Result.Key,
        url: s3Result.Location,
        size: glbBuffer.length,
        extension: ".glb",
      } as any)
  
      res.status(200).send(format)
    }
  } catch (e) {
    console.error(e)
    res.status(500).send("Error converting: " + e)
  }
}
