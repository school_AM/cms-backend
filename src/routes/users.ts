import {Request, Response} from "express";
import {User, USER_GROUP_ID} from "../data/model/entity/user";
import CRUDHelper from "./helpers/CRUDHelper";
import UserService from "../data/service/userService";
import {getGroupId, isAdmin} from "../utils/auth";

const CRUD = new CRUDHelper<User>(new UserService())

const TableName = "user"

export const createUser = (req: Request, res: Response) => {
  const { name } = req.body;
  if (typeof name !== "string") {
    res.status(400).json({ error: '"name" must be a string' });
    return
  }

  CRUD.create(name, req, res)
}

export const getUser = (req: Request, res: Response) => {
  CRUD.get(
    TableName,
    req,
    res,
    (user) => {
      delete user.password
      return user
    })
}

export const getAllUsers = (req: Request, res: Response) => {
  
  if (!isAdmin(res)) {
    if (!req.query.filter) {
      req.query.filter = {}
    }
    
    req.query.filter[USER_GROUP_ID] = getGroupId(res)
  }
  
  CRUD.getAll(
    TableName,
    req,
    res,
    (users) => {
      users.forEach((user) => delete user.password)
      return users
    })
}

export const updateUser = (req: Request, res: Response) => {
  CRUD.update(
    TableName,
    req,
    res,
    User.getParams(),
    (user) => {
      delete user.password
      return user
    })
}

export const deleteUser = (req: Request, res: Response) => {
  CRUD.remove(TableName, req, res)
}
