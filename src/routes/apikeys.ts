import {Request, Response} from "express";
import {ApiKey, APIKEY_GROUP_ID} from "../data/model/entity/apiKey";
import ApiKeyService from "../data/service/apiKeyService";
import {getGroupId, isAdmin} from "../utils/auth";
import AuthCRUDHelper from "./helpers/AuthCrudHelper";
import {generateApiToken} from "./authenticator";

const CRUD = new AuthCRUDHelper(new ApiKeyService())

const name = "API key"

export const createApiKey = (req: Request, res: Response) => {
  
  if (!isAdmin(res)) {
    req.body.groupId = getGroupId(res)
  }
  
  req.body.value = generateApiToken(req, res)
  
  CRUD.create(name, req, res)
}

export const getApiKey = (req: Request, res: Response) => {
  CRUD.get(name, req, res)
}

export const getAllApiKeys = (req: Request, res: Response) => {
  
  if (!isAdmin(res)) {
    if (!req.query.filter) {
      req.query.filter = {}
    }
    
    req.query.filter[APIKEY_GROUP_ID] = getGroupId(res)
  }
  
  CRUD.getAll(name, req, res)
}

export const updateApiKey = (req: Request, res: Response) => {
  CRUD.update(name, req, res, ApiKey.getParams())
}

export const deleteApiKey = (req: Request, res: Response) => {
  CRUD.remove(name, req, res)
}
