import {Request, Response} from "express";
import CRUDHelper from "./helpers/CRUDHelper";
import GroupService from "../data/service/groupService";
import {Group} from "../data/model/entity/group";

const CRUD = new CRUDHelper(new GroupService())

const TableName = "group"

export const createGroup = (req: Request, res: Response) => {
  const { name } = req.body;
  if (typeof name !== "string") {
    res.status(400).json({ error: '"name" must be a string' });
    return
  }

  CRUD.create(name, req, res)
}

export const getGroup = (req: Request, res: Response) => {
  CRUD.get(TableName, req, res)
}

export const getAllGroups = (req: Request, res: Response) => {
  CRUD.getAll(TableName, req, res)
}

export const updateGroup = (req: Request, res: Response) => {
  CRUD.update(TableName, req, res, Group.getParams())
}

export const deleteGroup = (req: Request, res: Response) => {
  CRUD.remove(TableName, req, res)
}
