import {Request, Response} from "express";
import CRUDHelper from "./CRUDHelper";
import BaseGroupAuthEntity from "../../data/model/entity/baseGroupAuthEntity";
import {getGroupId, isAdmin} from "../../utils/auth";
import NotFoundError from "../../data/model/error/NotFoundError";

/**
 * Provides authorization layer over base CRUDHelper to ensure safe data access based on groupIds.
 */
export default class AuthCRUDHelper<T extends BaseGroupAuthEntity<T>> extends CRUDHelper<T> {
  
  public create(name: string, req: Request, res: Response): void {
    
    const object: T = req.body
  
    if (!isAdmin(res) && (!getGroupId(res) || getGroupId(res) !== object.groupId)) {
      res.status(403).send();
      return
    }
  
    super.create(name, req, res);
  }
  
  public get<E extends Error>(name: string, req: Request, res: Response, normalizer?: (o: T) => T): void {
  
    super.get(
      name,
      req,
      res,
      normalizer,
      (result) => {
        
        if (!isAdmin(res) && (!getGroupId(res) || getGroupId(res) !== result.groupId)) {
          res.status(403).send();
          
        } else {
          res.json(result)
        }
      });
  }
  
  public getAll(name: string, req: Request, res: Response, normalizer?: (o: T[]) => T[]): void {
  
    super.getAll(
      name,
      req,
      res,
      normalizer,
      (result) => {
        
        if (!isAdmin(res) && (!getGroupId(res) || result.find((it) => it.groupId !== getGroupId(res)))) {
          res.status(403).send();
    
        } else {
          res.json(result)
        }
      });
  }
  
  public update<E extends Error>(
    name: string,
    req: Request,
    res: Response,
    fields: any,
    normalizer?: (o: T) => T): void {
  
    // verify new object groupId
    const newObject: T = req.body
    if (!isAdmin(res) && (!getGroupId(res) || newObject.groupId !== getGroupId(res))) {
      res.status(403).send();
      
    } else {
      this.service.get(req.params.id)
        .then((oldObject) => {
      
          // verify old object groupId
          if (!isAdmin(res) && (!getGroupId(res) || oldObject.groupId !== getGroupId(res))) {
            res.status(403).send();
        
          } else {
            super.update(name, req, res, fields, normalizer);
          }
        })
        .catch((error: E) => {
          if (error instanceof NotFoundError) {
            res.status(404).json({error: name + " not found"});
          } else {
            res.status(400).json({error: "Could not get " + name});
          }
        })
    }
  }
  
  public remove<E extends Error>(
    name: string, req: Request,
    res: Response): void {
  
    this.service.get(req.params.id)
      .then((oldObject) => {
        
        // verify old object groupId
        if (!isAdmin(res) && (!getGroupId(res) || oldObject.groupId !== getGroupId(res))) {
          res.status(403).send();
        
        } else {
          super.remove(name, req, res);
        }
      })
      .catch((error: E) => {
        if (error instanceof NotFoundError) {
          res.status(404).json({error: name + " not found"});
        } else {
          res.status(400).json({error: "Could not get " + name});
        }
      })
  }
}
