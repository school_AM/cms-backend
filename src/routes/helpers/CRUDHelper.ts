import {Request, Response} from "express";
import BaseEntity from "../../data/model/entity/BaseEntity";
import NotFoundError from "../../data/model/error/NotFoundError";
import BaseService from "../../data/service/baseService";
import BaseRepository from "../../data/repository/baseRepository";

export default class CRUDHelper<T extends BaseEntity<T>> {
  
  public service: BaseService<T, BaseRepository<T>>
  
  constructor(service: BaseService<T, BaseRepository<T>>) {
    this.service = service
  }
  
  public create(name: string, req: Request, res: Response) {
    this.service.create(req.body)
      .then((result) => {
        console.log("CRUD", result)
        res.json(result);
      })
      .catch((error) => {
        console.log("CRUD", error)
        res.status(400).json({ error: "Could not create " + name  });
      })
  }
  
  public get<E extends Error>(
    name: string,
    req: Request,
    res: Response,
    normalizer?: (o: T) => T,
    onResult?: (o: T) => void) {
    
    this.service.get(req.params.id)
      .then((result) => {
        
        if (normalizer) {
          result = normalizer(result)
        }
        
        if (onResult) {
          onResult(result)
        } else {
          res.json(result)
        }
      })
      .catch((error: E) => {
        if (error instanceof NotFoundError) {
          res.status(404).json({ error: name + " not found" });
        } else {
          res.status(400).json({ error: "Could not get " + name });
        }
      })
  }
  
  public getAll(
    name: string,
    req: Request,
    res: Response,
    normalizer?: (o: T[]) => T[],
    onResult?: (o: T[]) => void) {
    
    // get filter params from URL
    if (!req.query.filter) {
      req.query.filter = {}
    }
    
    this.service.getParams().forEach((param) => {
      if (req.query[param] && !req.query.filter[param]) {
        req.query.filter[param] = req.query[param]
      }
    })
    
    this.service.getAll(req.query.filter)
      .then((result) => {
        
        res.set("X-Total-Count", result.length + "")
        
        if (normalizer) {
          result = normalizer(result)
        }
  
        if (onResult) {
          onResult(result)
        } else {
          res.json(result)
        }
      })
      .catch((error) => {
        console.log(error)
        res.status(400).json({error: "Could not get " + name + "s"});
      })
  }
  
  public update<E extends Error>(
    name: string,
    req: Request,
    res: Response,
    fields: any,
    normalizer?: (o: T) => T) {
    
    this.service.update({ ...req.body, id:  req.params.id }, fields)
      .then((result) => {
        if (normalizer) {
          result = normalizer(result)
        }
        res.json(result);
      })
      .catch((error) => {
        if (error instanceof NotFoundError) {
          res.status(404).json({ error: name + " not found" });
        } else {
          res.status(400).json({ error: "Could not update " + name });
        }
      })
  }
  
  public remove(name: string, req: Request, res: Response) {
    this.service.remove(req.params.id)
      .then((result) => {
        res.status(200).json({ id: req.params.id })
      })
      .catch((error) => {
        res.status(400).json({ error: "Unable to delete item." });
      })
  }
}
