import {Request, Response} from "express";
import {Model, MODEL_GROUP_ID} from "../data/model/entity/model";
import ModelService from "../data/service/modelService";
import {getGroupId, isAdmin} from "../utils/auth";
import AuthCRUDHelper from "./helpers/AuthCrudHelper";

const CRUD = new AuthCRUDHelper(new ModelService())

const TableName = "model"

export const createModel = (req: Request, res: Response) => {
  const { name } = req.body;
  if (typeof name !== "string") {
    res.status(400).json({ error: '"name" must be a string' });
    return
  }
  
  if (!isAdmin(res)) {
    req.body.groupId = getGroupId(res)
  }

  CRUD.create(TableName, req, res)
}

export const getModel = (req: Request, res: Response) => {
  CRUD.get(TableName, req, res)
}

export const getAllModels = (req: Request, res: Response) => {
  
  if (!isAdmin(res)) {
    if (!req.query.filter) {
      req.query.filter = {}
    }
    
    req.query.filter[MODEL_GROUP_ID] = getGroupId(res)
  }
  
  CRUD.getAll(TableName, req, res)
}

export const updateModel = (req: Request, res: Response) => {
  CRUD.update(TableName, req, res, Model.getParams())
}

export const deleteModel = (req: Request, res: Response) => {
  CRUD.remove(TableName, req, res)
}
