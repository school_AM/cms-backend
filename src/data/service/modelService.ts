import BaseService from "./baseService";
import ModelRepository from "../repository/modelRepository";
import {Model} from "../model/entity/model";

export default class ModelService extends BaseService<Model, ModelRepository> {
  
  constructor() {
    super(new ModelRepository())
  }
  
  public getParams() {
    return Model.getParams()
  }
}
