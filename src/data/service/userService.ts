import BaseService from "./baseService";
import {User} from "../model/entity/user";
import UserRepository from "../repository/userRepository";

export default class UserService extends BaseService<User, UserRepository> {
  
  constructor() {
    super(new UserRepository())
  }
  
  public getParams() {
    return User.getParams()
  }
  
  public findByEmail(email: string): Promise<User> {
    return this.repository.findByEmail(email)
  }
}
