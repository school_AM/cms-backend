import BaseService from "./baseService";
import ApiKeyRepository from "../repository/apiKeyRepository";
import {ApiKey} from "../model/entity/apiKey";

export default class ApiKeyService extends BaseService<ApiKey, ApiKeyRepository> {
  
  constructor() {
    super(new ApiKeyRepository())
  }
  
  public getParams() {
    return ApiKey.getParams()
  }
}
