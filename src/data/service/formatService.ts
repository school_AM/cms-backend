import BaseService from "./baseService";
import FormatRepository from "../repository/formatRepository";
import Format from "../model/entity/format";
import {getUploadUrl, remove, S3_BUCKET} from "../s3/S3Service";

export default class FormatService extends BaseService<Format, FormatRepository> {
  
  constructor() {
    super(new FormatRepository())
  }
  
  public create(data: Format): Promise<Format> {
    
    data.url = `https://${S3_BUCKET}.s3.eu-central-1.amazonaws.com/${data.s3Key}`
    data.extension = data.s3Key.substring(data.s3Key.indexOf("."))
    
    return super.create(data)
      .then((format) => {
        // @ts-ignore
        format.uploadUrl = getUploadUrl(format.s3Key)
        return format
      })
  }
  
  public async remove(id: string): Promise<{}> {
    
    // get format and remove data from S3
    const format = await this.get(id)
    await remove(format.s3Key)
    
    return super.remove(id);
  }
  
  public getParams() {
    return Format.getParams()
  }
}
