import BaseService from "./baseService";
import GroupRepository from "../repository/groupRepository";
import {Group} from "../model/entity/group";

export default class GroupService extends BaseService<Group, GroupRepository> {
  
  constructor() {
    super(new GroupRepository())
  }
  
  public getParams() {
    return Group.getParams()
  }
}
