import BaseEntity from "../model/entity/BaseEntity";
import BaseRepository from "../repository/baseRepository";

export default abstract class BaseService<T extends BaseEntity<T>, R extends BaseRepository<T>> {
  
  protected repository: R
  
  constructor(repo: R) {
    this.repository = repo
  }
  
  public abstract getParams(): string[]
  
  public create(data: T): Promise<T> {
    return this.repository.create(data)
  }
  
  public get(id: string): Promise<T> {
    return this.repository.get(id)
  }
  
  public getAll(filter?: any): Promise<T[]> {
    return this.repository.getAll(filter)
  }
  
  public update(data: T, fields: any): Promise<T> {
    return this.repository.update(data, data.id, fields ? fields : Object.keys(data))
  }
  
  public remove(id: string): Promise<{}> {
    return this.repository.remove(id)
  }
}
