import {AuthType} from "./authType";

export default interface JwtPayload {
  type: AuthType
  user?: {
    id: string,
    email: string,
  }
  group?: {
    id: string,
  }
}
