export default abstract class BaseEntity<T extends BaseEntity<T>> {
  
  public static getParams(): string[] {
    return ["id"]
  }
  
  public id: string
  
  constructor(o: T) {
    this.id = o.id
  }
  
  public toPlainObj(): object {
    return Object.assign({}, this);
  }
}
