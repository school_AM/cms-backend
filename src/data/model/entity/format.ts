import BaseGroupAuthEntity from "./baseGroupAuthEntity";

export const FORMATS_TABLE = process.env.FORMATS_TABLE!

export const FORMAT_EXTENSION = process.env.FORMAT_EXTENSION!
export const FORMAT_MODEL_ID = process.env.FORMAT_MODEL_ID!
export const FORMAT_GROUP_ID = process.env.FORMAT_GROUP_ID!
export const FORMAT_S3_KEY = process.env.FORMAT_S3_KEY!
export const FORMAT_URL = process.env.FORMAT_URL!
export const FORMAT_SIZE = process.env.FORMAT_SIZE!

export default class Format extends BaseGroupAuthEntity<Format> {
  
  public static getParams(): string[] {
    return [...new Set(super.getParams()
      .concat([FORMAT_EXTENSION, FORMAT_GROUP_ID, FORMAT_MODEL_ID, FORMAT_S3_KEY, FORMAT_URL, FORMAT_SIZE]))]
  }
  
  public extension: string;
  public modelId: string;
  public s3Key: string
  public url: string
  public size: number
  
  constructor(o: Format) {
    super(o)
    
    this.extension = o.extension;
    this.modelId = o.modelId;
    this.s3Key = o.s3Key
    this.url = o.url
    this.size = o.size
  }
}
