import BaseGroupAuthEntity from "./baseGroupAuthEntity";

export const MODELS_TABLE = process.env.MODELS_TABLE!;

export const MODEL_NAME = process.env.MODEL_NAME!;
export const MODEL_GROUP_ID = process.env.MODEL_GROUP_ID!;

export class Model extends BaseGroupAuthEntity<Model> {
  
  public static getParams(): string[] {
    return [...new Set(super.getParams()
      .concat([MODEL_NAME, MODEL_GROUP_ID]))]
  }
  
  public name: string
  
  constructor(o: Model) {
    super(o)
    
    this.name = o.name
  }
}
