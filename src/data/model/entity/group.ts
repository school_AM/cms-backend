import BaseEntity from "./BaseEntity";

export const GROUPS_TABLE = process.env.GROUPS_TABLE!;

export const GROUP_NAME = process.env.GROUP_NAME!;

export class Group extends BaseEntity<Group> {
  
  public static getParams(): string[] {
    return [...new Set(super.getParams()
      .concat([GROUP_NAME]))]
  }
  
  public name: string
  
  constructor(o: Group) {
    super(o)
    
    this.name = o.name
  }
}
