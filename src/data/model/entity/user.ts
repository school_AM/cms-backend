import {compareSync, hashSync} from "bcryptjs";
import BaseGroupAuthEntity from "./baseGroupAuthEntity";

export const USERS_TABLE = process.env.USERS_TABLE!;

export const USER_NAME = process.env.USER_NAME!;
export const USER_EMAIL = process.env.USER_EMAIL!;
export const USER_PASSWORD = process.env.USER_PASSWORD!;
export const USER_ROLES = process.env.USER_ROLES!;
export const USER_GROUP_ID = process.env.USER_GROUP_ID!;

export class User extends BaseGroupAuthEntity<User> {
  
  public static getParams(): string[] {
    return [...new Set(super.getParams()
      .concat([USER_NAME, USER_EMAIL, USER_PASSWORD, USER_ROLES, USER_GROUP_ID]))]
  }
  
  public name: string
  
  public email: string
  public password: string
  
  public roles: UserRole[]
  
  constructor(o: User) {
    super(o)
    
    this.name = o.name
    this.email = o.email
    this.password = o.password
    this.roles = o.roles
  }
  
  public hashPassword() {
    this.password = hashSync(this.password, 10)
  }
  
  public validatePassword(password: string) {
    return compareSync(password, this.password)
  }
  
  public hasRole(role: UserRole) {
    return this.roles.findIndex((it) => it === role) > -1
  }
}

export enum UserRole {
  USER = "USER",
  ADMIN = "ADMIN",
}
