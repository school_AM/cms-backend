import BaseGroupAuthEntity from "./baseGroupAuthEntity";

export const APIKEYS_TABLE = process.env.APIKEYS_TABLE!;

export const APIKEY_GROUP_ID = process.env.APIKEY_GROUP_ID!;
export const APIKEY_VALUE = process.env.APIKEY_VALUE!;
export const APIKEY_NAME = process.env.APIKEY_NAME!;

export class ApiKey extends BaseGroupAuthEntity<ApiKey> {
  
  public static getParams(): string[] {
    return [...new Set(super.getParams()
      .concat([APIKEY_VALUE, APIKEY_GROUP_ID, APIKEY_NAME]))]
  }
  
  public value: string
  public name: string
  
  constructor(o: ApiKey) {
    super(o)
    
    this.value = o.value
    this.name = o.name
  }
}
