import BaseEntity from "./BaseEntity";

export default abstract class BaseGroupAuthEntity<T extends BaseGroupAuthEntity<T>> extends BaseEntity<T> {
  
  public static getParams(): string[] {
    return super.getParams().concat(["groupId"])
  }
  
  public groupId: string
  
  constructor(o: T) {
    super(o)
    
    this.groupId = o.groupId
  }
}
