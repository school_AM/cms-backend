import AWS = require("aws-sdk");

const s3 = new AWS.S3();
export const S3_BUCKET = process.env.S3_BUCKET!

export const upload = async (key: string, body: Buffer) => {
  return s3.upload({
    Bucket: S3_BUCKET,
    Key: key,
    Body: body,
  }).promise()
}

export const getUploadUrl = (key: string) => {
  return s3.getSignedUrl("putObject", {
    Key: key,
    Bucket: S3_BUCKET,
  })
}

export const get = async (key: string) => {
  return s3.getObject({
    Bucket: S3_BUCKET,
    Key: key,
  }).promise()
}

export const remove = async (key: string) => {
  return s3.deleteObject({
    Bucket: S3_BUCKET,
    Key: key,
  }).promise()
}
