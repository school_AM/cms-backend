import BaseRepository from "./baseRepository";
import {ApiKey, APIKEYS_TABLE} from "../model/entity/apiKey";

export default class ApiKeyRepository extends BaseRepository<ApiKey> {
  
  constructor() {
    super(APIKEYS_TABLE)
  }
  
  public create(data: ApiKey): Promise<ApiKey> {
    return super.create(new ApiKey(data))
      .then((it) => new ApiKey(it))
  }
  
  public get(id: string): Promise<ApiKey> {
    return super.get(id)
      .then((it) => new ApiKey(it))
  }
  
}
