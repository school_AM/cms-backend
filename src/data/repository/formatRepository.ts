import BaseRepository from "./baseRepository";
import Format, {FORMATS_TABLE} from "../model/entity/format";

export default class FormatRepository extends BaseRepository<Format> {
  
  constructor(){
    super(FORMATS_TABLE)
  }
  
  public create(data: Format): Promise<Format> {
    return super.create(new Format(data))
      .then((it) => new Format(it))
  }
  
  public get(id: string): Promise<Format> {
    return super.get(id)
      .then((it) => new Format(it))
  }
}
