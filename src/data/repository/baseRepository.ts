import {dynamoDb} from "../database/dynamoDb";
import * as uuid from "uuid";
import {GetItemOutput, ScanOutput} from "aws-sdk/clients/dynamodb";
import BaseEntity from "../model/entity/BaseEntity";
import NotFoundError from "../model/error/NotFoundError";
import {AWSError} from "aws-sdk";
import {getFilterQuery} from "../../utils/QueryHelper";

export default abstract class BaseRepository<T extends BaseEntity<T>> {
  
  public table: string
  
  constructor(table: string) {
    this.table = table
  }
  
  public create(data: T): Promise<T> {
    
    data.id = uuid()
    
    const params = {
      TableName: this.table,
      Item: {
        ...data.toPlainObj(),
      },
    };
    
    return new Promise((resolve, reject) => {
      dynamoDb.put(params, (error, result) => {
        console.log("CREATE DONE", error, result)
        if (error) {
          console.log(error);
          reject(error)
        } else {
          this.get(data.id)
            .then((res: T) => {
              resolve(res)
            })
            .catch((err) => {
              reject(err)
            })
        }
      });
    })
  }
  
  public get(id: string): Promise<T> {
    const params = {
      TableName: this.table,
      Key: {
        id: id,
      },
    }
    
    return new Promise((resolve, reject) => {
      dynamoDb.get(params, (error: AWSError, result: GetItemOutput) => {
  
        console.log("GET DONE", error, result)
        if (error) {
          console.log(error);
          reject(error)
        } else if (result.Item) {
          resolve(result.Item as unknown as T)
        } else {
          reject(new NotFoundError())
        }
      });
    })
  }
  
  public getAll(filter: any): Promise<T[]> {
    
    // TODO range
    // TODO sort
    
    let params: any
    if (filter && Object.keys(filter).length !== 0) {
      params = getFilterQuery(this.table, filter)
      
    } else {
      params = {
        TableName: this.table,
      }
    }
    
    return new Promise((resolve, reject) => {
      dynamoDb.scan(params, (err, data: ScanOutput) => {
        if (err) {
          console.log(err);
          reject(err)
        } else {
          // @ts-ignore
          resolve(data.Items ? data.Items : [])
        }
      })
    })
  }
  
  public update(data: T, id: string, fields: any): Promise<T> {
    
    const paramsGet = {
      TableName: this.table,
      Key: {
        id: id,
      },
    }
    
    return new Promise((resolve, reject) => {
      
      // Get user old data
      dynamoDb.get(paramsGet, (error, result) => {
  
        if (error) {
          console.log(error);
          reject(error)
          
        } else if (result.Item) {
          
          const oldData = result.Item;
          const updateParams = {
            TableName: this.table,
            Key: {
              id,
            },
            ...this.getUpdateParams(fields, oldData, data),
            ReturnValues: "ALL_NEW",
          };
          
          console.log("params", updateParams)
          // Update data
          dynamoDb.update(updateParams, (err, res) => {
  
            console.log("update update done", res, err)
            if (err) {
              console.log(err);
              reject(err)
            } else {
              resolve(res.Attributes as T)
            }
          });
          
        } else {
          reject(new NotFoundError())
        }
      });
    })
  }
  
  public remove(id: string): Promise<{}> {
    const params = {
      TableName: this.table,
      Key: {
        id: id,
      },
    };
    
    return new Promise((resolve, reject) => {
      dynamoDb.delete(params, (error) => {
        if (error) {
          console.log(error);
          reject(error)
        } else {
          resolve({})
        }
      })
    })
  }
  
  private getUpdateParams(fields: any, oldData: any, newData: any) {
    let UpdateExpression = "set";
    const ExpressionAttributeNames = {};
    const ExpressionAttributeValues = {};
    
    fields
      .filter((it: string) => it && it !== "id")
      .forEach((field: string) => {
      
      ExpressionAttributeValues[":" + field + "_val"] = newData[field] ? newData[field] : oldData[field]
      if (ExpressionAttributeValues[":" + field + "_val"]) {
        ExpressionAttributeNames["#" + field + "_key"] = field
        UpdateExpression += " #" + field + "_key = :" + field + "_val,"
      }
    })
    UpdateExpression = UpdateExpression.slice(0, UpdateExpression.length - 1)
    
    return {UpdateExpression, ExpressionAttributeValues, ExpressionAttributeNames}
  }
}
