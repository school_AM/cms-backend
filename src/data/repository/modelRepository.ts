import BaseRepository from "./baseRepository";
import {Model, MODELS_TABLE} from "../model/entity/model";

export default class ModelRepository extends BaseRepository<Model> {
  
  constructor() {
    super(MODELS_TABLE)
  }
  
  public create(data: Model): Promise<Model> {
    return super.create(new Model(data))
      .then((it) => new Model(it))
  }
  
  public get(id: string): Promise<Model> {
    return super.get(id)
      .then((it) => new Model(it))
  }
}
