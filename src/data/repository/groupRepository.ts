import BaseRepository from "./baseRepository";
import {Group, GROUPS_TABLE} from "../model/entity/group";

export default class GroupRepository extends BaseRepository<Group> {
  
  constructor() {
    super(GROUPS_TABLE)
  }
  
  public create(data: Group): Promise<Group> {
    return super.create(new Group(data))
      .then((it) => new Group(it));
  }
  
  public get(id: string): Promise<Group> {
    return super.get(id)
      .then((it) => new Group(it));
  }
}
