import BaseRepository from "./baseRepository";
import {User, USER_EMAIL, USERS_TABLE} from "../model/entity/user";
import {dynamoDb} from "../database/dynamoDb";
import {ScanOutput} from "aws-sdk/clients/dynamodb";
import NotFoundError from "../model/error/NotFoundError";

export default class UserRepository extends BaseRepository<User> {
  
  constructor() {
    super(USERS_TABLE)
  }
  
  public async create(data: User): Promise<User> {
    
    let user
    
    try {
      user = await this.findByEmail(data.email)
    } catch (e) {}
  
    if (user) {
      throw Error("Email already exists.")
    
    } else {
      user = new User(data)
      user.hashPassword()
    
      return super.create(user)
        .then((it) => new User(it))
    }
    
  }
  
  public get(id: string): Promise<User> {
    return super.get(id)
      .then((it) => new User(it))
  }
  
  public findByEmail(email: string): Promise<User> {
    
    const params = {
      TableName: this.table,
      FilterExpression: `#key = :value`,
      ExpressionAttributeNames: {
        "#key": USER_EMAIL,
      },
      ExpressionAttributeValues: {
        ":value": email,
      },
    }
    
    return new Promise((resolve, reject) => {
      dynamoDb.scan(params, (error, result: ScanOutput) => {
        if (error) {
          console.log(error);
          reject(error)
        } else if (result.Items && result.Items.length > 0) {
          resolve(new User(result.Items![0] as unknown as User))
        } else {
          reject(new NotFoundError())
        }
      });
    })
  }
}
