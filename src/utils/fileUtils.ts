
export function dataURLtoBuffer(dataUrl: string, fileName: string): Buffer {
  return Buffer.from(dataUrl.split(",")[1], "base64")
}
