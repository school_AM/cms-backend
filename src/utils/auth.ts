import {Response} from "express";
import {User, UserRole} from "../data/model/entity/user";
import JwtPayload from "../data/model/auth/jwtPayload";
import {AuthType} from "../data/model/auth/authType";

export function getUser(response: Response): User | undefined {
  return response.locals.user
}

export function getGroupId(response: Response): string | undefined {
  const jwtPayload: JwtPayload = response.locals.jwtPayload
  
  if (jwtPayload.type === AuthType.USER) {
    const user = getUser(response)
    return user ? user.groupId : undefined
  } else {
    return jwtPayload.group!.id
  }
}

export function isAdmin(response: Response) {
  const user: User = response ? response.locals.user : undefined
  
  return user && user.roles.includes(UserRole.ADMIN)
}
