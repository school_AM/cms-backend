import { parse } from "relaxed-json";

// TODO ? does it works ?
export const getQueryParams = (query: any) => {

  if (!query) { return {} }

  let filter, range, sort

  if (query.filter) {
    const filterData = parse(query.filter)
    filter = {field: Object.keys(filterData)[0], value: Object.values(filterData)[0]}
  }

  if (query.range) {
    range = query.range.slice(1, query.range.length - 1).split(", ")
  }

  if (query.sort) {
    sort = query.sort.slice(1, query.sort.length - 1).replace(/'/g, "").split(",")
  }

  return {filter, range, sort, ...query}
}

/**
 * Build filter DB query based on filter object.
 * Joins each item to query by AND operator. If item is array of values,
 * this values are connected by OR operator.
 *
 * @param {string} table
 * @param filter
 * @return {{TableName: string; FilterExpression: string; ExpressionAttributeNames: {}; ExpressionAttributeValues: {}}}
 */
export const getFilterQuery = (table: string, filter: any) => {
  
  const params = {
    TableName: table,
    FilterExpression: "",
    ExpressionAttributeNames: {
    },
    ExpressionAttributeValues: {
    },
  }
  
  const keys = Object.keys(filter)
  
  // holders for inner forEach - for array filter values
  const ExpressionAttributeNames = {}
  const ExpressionAttributeValues = {}
  
  // filter expressions
  const FilterExpression: string[] = []
  
  // bind filter values from keys
  keys.forEach((it, i) => {
    
    if (filter[it] instanceof Array) {
      
      // inner filter expressions
      const innerFilterExpression: string[] = []
      
      filter[it].forEach((value: string, j: number) => {
        ExpressionAttributeNames["#key" + i + j] = it
        ExpressionAttributeValues[":value" + i + j] = value
        
        innerFilterExpression.push(`#key${"" + i + j} = :value${"" + i + j}`)
      })
      
      FilterExpression.push(`(${innerFilterExpression.join(" OR ")})`)
      
    } else {
      params.ExpressionAttributeNames["#key" + i] = it
      params.ExpressionAttributeValues[":value" + i] = filter[it]
      
      FilterExpression.push(`#key${i} = :value${i}`)
    }
  })
  
  // build filter expression
  params.FilterExpression = FilterExpression.join(" AND ")
  
  // connect base and inner values
  params.ExpressionAttributeNames = { ...params.ExpressionAttributeNames, ...ExpressionAttributeNames }
  params.ExpressionAttributeValues = { ...params.ExpressionAttributeValues, ...ExpressionAttributeValues }
  
  return params
}
