## Setup

`npm install` to install all packages

`npm i serverless -g` install serverless tool globally

Config for deployment to AWS using Serverless CLI tool is prepared in `serverless.yml` file. 
But it's necessary to set up AWS credentials on your own. See tutorial at: [https://serverless.com/framework/docs/providers/aws/guide/credentials/](https://serverless.com/framework/docs/providers/aws/guide/credentials/)

## Run in local environment

In the project directory, you can run:

### `npm run 'deploy localy'`

Runs the app in the development mode localy using serverless-offline plugin.

!!! Correct set-up of AWS credentials for serverless tool is necessary. There are used services which can not be emulated locally. !!!

### Deployment

`npm run deploy` Deploys app using serverless plugin by using existing config.
